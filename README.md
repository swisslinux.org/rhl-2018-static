RHL 2018, static copy of the website
====================================

This is a static copy of the website of the 2018 verson of Rencontres
Hivernales du Libre.



Build
-----

With Podman:
```sh
podman build -t 2018.hivernal.es:latest .
```


With Docker:
```sh
docker build -t 2018.hivernal.es:latest .
```


RUN
---

With Podman:
```sh
podman run 2018.hivernal.es:latest
```

With Docker:
```sh
docker run 2018.hivernal.es:latest
```


Author
------

Sébastien Gendre <seb@k-7.ch>
